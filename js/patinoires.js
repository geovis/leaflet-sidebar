var patinoires = [
  {
    nom_equipe: "EHC Biel-Bienne",
    nom_patinoire: "Tissot Arena",
    adresse: "Boulevard des Sports 18, 2504 Biel",
    places: 6500,
    web: "http://www.ehcb.ch",
    coords: [47.15564, 7.28128]
  },
  {
    nom_equipe: "EV Zug",
    nom_patinoire: "Bossard Arena",
    adresse: "General-Guisan-Strasse 4, 6303 Zug",
    places: 7200,
    web: "https://www.evz.ch",
    coords: [47.17679, 8.50759]
  },
  { 
    nom_equipe: "Fribourg-Gottéron",
    nom_patinoire: "Banque Cantonale Fribourgeoise Arena",
    adresse: "Allée du Cimetière 1, 1700 Fribourg",
    places: 6900,
    web: "https://www.gotteron.ch",
    coords: [46.81736, 7.15551]
  },
  {
    nom_equipe: "Genève-Servette HC",
    nom_patinoire: "Patinoire des Vernets",
    adresse: "Rue Hans-Wilsdorf 4, 1227 Genève",
    places: 7135,
    web: "https://www.gshc.ch",
    coords: [46.19450, 6.13301]
  },
  { nom_equipe: "HC Ambri Piotta",
    nom_patinoire: "Pista della Valascia",
    adresse: "Via Valascia, 6775 Quinto",
    places: 6500,
    web: "https://www.hcap.ch",
    coords: [46.50920, 8.69192]
  },
  {
    nom_equipe: "HC Davos",
    nom_patinoire: "Vaillant Arena",
    adresse: "Eisbahnstrasse 5, 7270 Davos",
    places: 7080,
    web: "https://www.hcd.ch",
    coords: [46.79851, 9.82639]
  },
  {
    nom_equipe: "HC Lugano",
    nom_patinoire: "Cornèr Arena (La Resega)",
    adresse: "Via Chiosso 8, 6948 Porza",
    places: 7800,
    web: "https://www.hclugano.ch/",
    coords: [46.02631, 8.96346]
  },
  {
    nom_equipe: "Lausanne HC",
    nom_patinoire: "Malley SA 2.0",
    adresse: "Avenue du Chablais 16-18, 1008 Prilly",
    places: 6700,
    web: "http://www.lausannehc.ch/",
    coords: [46.52567, 6.60184]
  },
  {
    nom_equipe: "SC Bern",
    nom_patinoire: "PostFinance Arena",
    adresse: "Mingerstrasse 12, 3014 Bern",
    places: 17031,
    web: "https://www.scb.ch",
    coords: [46.95854, 7.46865]
  },
  {
    nom_equipe: "SC Rapperswil-Jona Lakers",
    nom_patinoire: "Sankt Galler Kantonalbank Arena",
    adresse: "Walter-Denzler-Strasse 3, 8640 Rapperswil ",
    places: 6100,
    web: "https://www.lakers.ch",
    coords: [47.22311, 8.82546]
  },
  {
    nom_equipe: "SCL Tigers",
    nom_patinoire: "Ilfishalle",
    adresse: "Güterstrasse 14, 3550 Langnau i.E.",
    places: 6000,
    web: "https://www.scltigers.ch",
    coords: [46.93624, 7.78610]
  },
  {
    nom_equipe: "ZSC Lions",
    nom_patinoire: "Hallenstadion",
    adresse: "Wallisellenstrasse 45, 8050 Zürich",
    places: 11200,
    web: "https://www.zsclions.ch",
    coords: [47.41145, 8.55167]
  }
];
