var map = L.map('map');
map.setView({ lat: 46.7, lng: 7.05 }, 9);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
}).addTo(map);

L.control.scale().addTo(map);


// Ajouter la boîte latérale
var sidebar = L.control.sidebar('sidebar', {
    position: 'left'
});
map.addControl(sidebar);


// Ajouter les marqueurs pour les patinoires
for (var i=0; i < patinoires.length; i++){
  ajouter_patinoire(patinoires[i]);
}

map.on('click', function(){ sidebar.hide(); })


function ajouter_patinoire(pat){
  var m = L.marker(pat.coords).addTo(map);
  m.patinoire = pat;
  m.on('click', function(e){
    var p = e.target.patinoire;
    sidebar.setContent(`
      <h2>${p.nom_equipe}</h2>
      <p><b>${p.nom_patinoire}</b><br />${p.adresse}</p>
      <p>${p.places} places</p>
      <p><a href="${p.web}">${p.web}</a></p>
    `);
    sidebar.show();
  })
}
